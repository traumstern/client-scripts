#!/usr/bin/env bash
# Copyright 2020 Kino Traumstern
# SPDX-License-Identifier: Apache-2.0

SCRIPT_PATH=$(realpath "${BASH_SOURCE[0]}")
SCRIPT_DIR=$(dirname "${SCRIPT_PATH}")

# shellcheck source=assert.bash
source "${SCRIPT_DIR}/assert.bash"

assert_ssh_removed() {
  local folder=${1}
  local expected_files=("${@:2}")

  local file
  for file in "${expected_files[@]}"; do
    assert_output_contains "ssh\ traumstern\ rm\ \S*${folder}/${file}"
  done
}

assert_ssh_not_removed() {
  local folder=${1}
  local expected_files=("${@:2}")

  local file
  for file in "${expected_files[@]}"; do
    assert_output_not_contains "ssh\ traumstern\ rm\ \S*${folder}/${file}"
  done
}

assert_ssh_moved() {
  local source=${1}
  local target=${2}

  assert_output_contains "ssh\ traumstern\ mv\ .*${source} ${target}"
}
