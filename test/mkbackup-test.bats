#!/usr/bin/env bats
# Copyright 2020 Kino Traumstern
# SPDX-License-Identifier: Apache-2.0

load helpers
load assert
load ../bash/settings

setup() {
  wipe_working_directory
  wipe_remote_folder
  create_remote_production_folder
}

CMD="/code/bash/update-website.sh"

@test "It creates a normal, daily, backup when invoked without arguments" {
  local date_formatted=$(get_date_formatted)
  create_remote_folder $(get_backup_folder_name '')

  run "${CMD}" mkbackup
  assert_backup_created "${date_formatted}"
}

@test "It creates a suffixed backup when invoked with suffix argument" {
  local date_formatted=$(get_date_formatted)
  create_remote_folder $(get_backup_folder_name '')

  run "${CMD}" mkbackup my-manual-suffix
  assert_backup_created "${date_formatted}-my-manual-suffix"
}
