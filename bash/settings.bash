#!/usr/bin/env bash
# Copyright 2020 Kino Traumstern
# SPDX-License-Identifier: Apache-2.0

get_backup_folder_name() {
  local backup_name="${1}"
  echo ".kino-backups/content/${backup_name}"
}

get_date_formatted() {
  local date=${1:-}
  local format_pattern="${2:-"%d-%m-%Y"}"
  if [[ -n ${date} ]]; then
    date --date="${date}" +"${format_pattern}"
  else
    date +"${format_pattern}"
  fi
}